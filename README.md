dot
===

personal dotfiles branched by machine

credits
===

using my favorite parts and inspiration from the masters:

[skwp](http://github.com/skwp/dotfiles)

[spf13](http://github.com/spf13/spf13-vim)

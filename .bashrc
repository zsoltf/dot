#
# zsolt's bashrc
#

# Startup scripts

# Shortcuts

alias df='df -h'
alias tree='tree -Ch'
alias ls='ls -h'
alias la='ls -a'
alias vrc='vim ~/.vimrc'
alias c='clear'

# vi
export EDITOR='vim'
set -o vi

# prompt
PS1="\u: [\W] $ "
export CLICOLOR=1
export LSCOLORS=CxFxCxDxBxegedabagaced
